var Pessoa = {
         inserir : function(_pessoa) {
                var settings = {
                       type : "POST",
                       url : 'api/pessoa/inserir',
                       data : JSON.stringify(_pessoa),
                       contentType : "application/json"
                };
                return $.ajax(settings);
        },

        inserirOk: function(data,  textStatus, jqXHR){
                   console.log("Status: " + jqXHR.status);
                   console.log("Location: " + jqXHR.getResponseHeader('Location'));
                   $("#global-message").addClass("alert-success").text("Pessoa com id = " + data + " criado com sucesso.").show();
        },
        inserirFail: function(request){
        
            $.each(request.responseJSON, function(index, value) {
                console.log(index);
                console.log(value);
            
            }); 
            
        },
    
    obter : function(id){
        return $.ajax({
                type : "GET",
                url : this.url + "/" + id
        });
    },
    
    buscar : function(){
        return $.ajax({
                type : "GET",
                url : 'api/pessoa' // /listar'
        });
             
    
    }
    
    
   
};


