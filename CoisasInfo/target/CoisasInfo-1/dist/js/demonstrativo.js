var MSG = {
    tit: 'Demonstrativo Consolidação',
    E1: 'Sistema temporariamente indisponível ou a sua sessão expirou por inatividade. Retorne à página inicial da aplicação para reiniciar.',
    E2: 'Contribuinte não possui pedidos passíveis de gerar demonstrativo de consolidação.',

    btn: {// Botão que volta para o início
        modal: true,
        resizable: false,
        buttons: {
            Ok: function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            window.location = "Inicio.aspx";
        }
    },
    btnRetornar: {// Botão que volta para o selecionar contribuinte
        modal: true,
        resizable: false,
        buttons: {
            Ok: function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            var niRet = $('#dvNiT1').text();
            SelContribClient.retornar('DemonstrativoConsolidacao.aspx', MSG.tit, "", "", DemonstrativoConsolidacao.NI.niF);

        }
    },
    btnErroRetornar: {
        modal: true,
        resizable: false,
        buttons: {
            Ok: function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            $('#dvTela2:visible').hide();
            DemonstrativoConsolidacao.MontarTela1();
        }
    },
    btnFocus: {
        modal: true,
        resizable: false,
        buttons: {
            Ok: function () {
                $(this).dialog('close');
            }
        },
        close: function () {
            $(MSG.elementFocus).focus();
        }
    },

    tratar_mensagens: function (cod, msg) {
       
        switch (cod) {
            case '0001', '0002', '0003', '0004': //Erro na chamada ao "O" 
                exibirErro(MSG.E1, MSG.btnRetornar);
            case '0005': //retornou vazio
                exibirErro(msg, MSG.btnErroRetornar);
                break;
            case '0006': // retornou nulo
                exibirErro(msg, MSG.btnErroRetornar);
                break;
            case '0099': //ERRO NATURAL NO MODULO
                exibirErro(MSG.E1, MSG.btnRetornar);
                break;
            case '1640': // Exceção no command
                exibirErro(MSG.E1, MSG.btnRetornar);
                break;
            case '9999': // Erro de cripto
                exibirErro(MSG.E1, MSG.btnRetornar);
                break;
            default:
                exibirErro(MSG.E1, MSG.btn);
                break;
        }
    }
};

//usado para ordenar
var ModalidadesArt39_40 = {
    "965": { peso: "0", CodModalidade: "965", NomeModalidade: "Lei 12.865/2013 - Art. 39, caput - à vista - RFB", CodSituacao: "00" },
    "961": { peso: "1", CodModalidade: "961", NomeModalidade: "Lei 12.865/2013 - Art. 39, caput - parcelado - RFB", CodSituacao: "00" },
    "966": { peso: "2", CodModalidade: "966", NomeModalidade: "Lei 12.865/2013 - Art. 39, §1º - à vista - RFB", CodSituacao: "00" },
    "962": { peso: "3", CodModalidade: "962", NomeModalidade: "Lei 12.865/2013 - Art. 39, §1º - parcelado - RFB", CodSituacao: "00" },
    "967": { peso: "4", CodModalidade: "967", NomeModalidade: "Lei 12.865/2013 - Art. 40 - à vista - RFB", CodSituacao: "00" },
    "968": { peso: "5", CodModalidade: "968", NomeModalidade: "Lei 12.865/2013 - Art. 40 - à vista - PGFN", CodSituacao: "00" },
    "963": { peso: "6", CodModalidade: "963", NomeModalidade: "Lei 12.865/2013 - Art. 40 - parcelado - RFB", CodSituacao: "00" },
    "964": { peso: "7", CodModalidade: "964", NomeModalidade: "Lei 12.865/2013 - Art. 40 - parcelado - PGFN", CodSituacao: "00" }
};

// Classe Javascript Criada para controlar a visão do caso de uso Demonstrativo Consolidação
var DemonstrativoConsolidacao = {
    Tela: {},
    Modalidade: {},
    NI: {},
    NomeNI: {},
    Usuario: {},
    nr_identificacao: "",
    // Método chamado no início do carregamento da página
    Iniciar: function () {
        $('#dvTela2').hide();
        $('#lnkVoltar').unbind().bind('click', DemonstrativoConsolidacao.voltarClick);

        $('.ajaxStatus').unbind().bind('ajaxStart', function () { $("span.ajaxStatus").show(); });
        $('.ajaxStatus').unbind().bind('ajaxStop', function () { $("span.ajaxStatus").hide(); });

        DemonstrativoConsolidacao.NI.tp = "1";
        DemonstrativoConsolidacao.NI.niF = $('#dvNiT1').text();
        DemonstrativoConsolidacao.NI.ni = DemonstrativoConsolidacao.NI.niF.replace(/[^0-9]/g, '');
        DemonstrativoConsolidacao.MontarTela1();

    },
    // Método para voltar para tela anterior
    voltarClick: function (event) {
        event.preventDefault();
        switch (DemonstrativoConsolidacao.Tela.codigo) {
            case 1:

                SelContribClient.retornar('DemonstrativoConsolidacao.aspx', MSG.tit, "", "", DemonstrativoConsolidacao.NI.niF);

                break;
            case 2:
                $('#dvTela2:visible').hide();
                DemonstrativoConsolidacao.MontarTela1();
                break;
        }
    },
    // Monta a lista de modalidades com links ativos para o caso de uso
    MontarTela1: function () {
        $('#dvMods').empty();
        DemonstrativoConsolidacao.recuperar_modalidades_evento_13();
        DemonstrativoConsolidacao.Tela.codigo = 1;
        $('#dvTela1').show();
        $('#fldMods').show();

        //limpando informações - tabelas de dados - caso do botão voltar
        DemonstrativoConsolidacao.limparDadosPrestacoes();
        DemonstrativoConsolidacao.limparDadosTabelaPFBCN();
    },
    //Monta a tela do Demonstrativo com todos os seu quadros
    MontarTela2: function () {
        $('#dvTela2').show();
        $('#nm_modalidade').text(DemonstrativoConsolidacao.Modalidade.nmMod);
        $('#data_consolidacao').text(Utils.AAAAMMDDToDDMMAAAA(DemonstrativoConsolidacao.Modalidade.dtCon, "true"));
        // Processos
        $('#dvlista_processo').hide();
        $('#dvTelaDividaConsolidadaRFB').hide();
        // Inscricoes
        $('#dvlista_inscricao').hide();
        $('#dvTelaDividaConsolidadaPGFN').hide();
        //
        DemonstrativoConsolidacao.Tela.codigo = 2;
        //
        if (DemonstrativoConsolidacao.isProcesso()) {
            $('#dvlista_processo').show();
            $('#dvTelaDividaConsolidadaRFB').show();
        }
        //
        if (DemonstrativoConsolidacao.isInscricao()) {
            $('#dvlista_inscricao').show();
            $('#dvTelaDividaConsolidadaPGFN').show();
        }
    },
    //Método para buscar modalidades com evento 13 
    recuperar_modalidades_evento_13: function () {
        var j = {}, p = {};
        j.TipoNi = DemonstrativoConsolidacao.NI.tp;
        j.NI = DemonstrativoConsolidacao.NI.ni;
        j.CodModalidade = "000";
        j.DataEvento = "00000000";
        j.HoraEvento = "0000000";
        j.TipoConsulta = 1;
        p.act = 'VerificarContaDemonstrativoArt39e40';
        p.dt = JSON.stringify(j);
        DAL.posting('AjajAdesaoManual.aspx', p, DemonstrativoConsolidacao.tratar_modalidades_retornadas, 'm');
    },

    //Método para tratar o retorno da  chamada o O348071F VerificarContaAdesaoManualCMD
    tratar_modalidades_retornadas: function () {
        var cr = DAL.cr.m, dt = DAL.data.m, msg;
        delete DAL.cr.m;
        delete DAL.data.m;
        if (cr !== '00') {
            $('#msgerr').text(cr + ' - ' + dt);
            MSG.tratar_mensagens(cr, dt);
        } else {
            if ($.type(dt.retorno.CodRetorno) !== "undefined") {
                if (dt.retorno.CodRetorno === "0000") {
                    DemonstrativoConsolidacao.montar_lista_modalidades(dt);
                } else {
                    $('#msgerr').text(dt.retorno.CodRetorno + ' - ' + dt.retorno.MsgErroRet);
                    exibirErro(MSG.E1, MSG.btnRetornar);
                }
            } else {
                $('#msgerr').text("erro desconhecido");
                exibirErro(MSG.E1, MSG.btnRetornar);
            }
        }
    },
    //Método para montar lista de modalidades, recebe como parâmetro os dados retornado do grande porte : lista de modalidades com evento 13
    montar_lista_modalidades: function (dt) {
        this.modalidadeComLink = false;
        DemonstrativoConsolidacao.modalidades_data = dt.modalidades_data;

        // Itera toda a lista de modalidades possíveis chama o método adicionar_modalidade para cada uma.       
        for (var key_mod in ModalidadesArt39_40) {
            DemonstrativoConsolidacao.adicionar_modalidade(ModalidadesArt39_40[key_mod]);
        }
        $('#dvMods .mod a').unbind().bind('click', DemonstrativoConsolidacao.modalidade_click);
    },
    //Método para criar o link ativo ou inativo para seleção
    adicionar_modalidade: function (mod) {
        var e = $('#dvMods');
        var modalidade_passada = mod.CodModalidade.toString();
        var tem_modalidade = false;

        for (var i = 0; i < this.modalidades_data.length; i++) {
            if (this.modalidades_data[i].Modalidade == modalidade_passada) {
                tem_modalidade = true;
                break;
            }
        }

        if (tem_modalidade == true) {
            e.append('<div class="mod"><a href="#" id="' + mod.CodModalidade + '">' + mod.NomeModalidade + '</a></div>');
            this.modalidadeComLink = true;
        } else {
            e.append('<div class="mod"><label title="" id="' + mod.CodModalidade + '">' + mod.NomeModalidade + '</label></div>');
        }

    },
    // Testa se é inscrição
    isInscricao: function () {
        var m = DemonstrativoConsolidacao.Modalidade.cdMod;
        return (m === '964' || m === '968');
    },
    // Testa se é processo
    isProcesso: function () {
        var m = DemonstrativoConsolidacao.Modalidade.cdMod;
        return (m === '961' || m === '962' || m === '963' || m === '965' || m === '966' || m === '967');
    },
    //Evento principal que dispara todas as outras chamadas
    modalidade_click: function (event) {
        event.preventDefault();
        $('#fldMods').hide();
        //
        DemonstrativoConsolidacao.Modalidade.cdMod = $(this).attr("id");
        DemonstrativoConsolidacao.Modalidade.nmMod = $(this).text();

        for (var i = 0; i < DemonstrativoConsolidacao.modalidades_data.length; i++) {
            if (DemonstrativoConsolidacao.modalidades_data[i].Modalidade == DemonstrativoConsolidacao.Modalidade.cdMod) {
                DemonstrativoConsolidacao.Modalidade.dtCon = DemonstrativoConsolidacao.modalidades_data[i].DataConsolidacao;
                break;
            }
        }

        DemonstrativoConsolidacao.gerar_lista_processos();

       // DemonstrativoConsolidacao.gerar_divida_consolidada();

      //  DemonstrativoConsolidacao.gerePrejuizoFiscal_BCN_CSLL();

     //   DemonstrativoConsolidacao.gereDadosPrestacao();

        //DemonstrativoConsolidacao.MontarTela2();
       // return false;
    },
    //Gera a lista débitos por processo
    gerar_lista_processos: function () {
        var j = {}, p = {};
        j.TipoNi = DemonstrativoConsolidacao.NI.tp;
        j.NI = DemonstrativoConsolidacao.NI.ni;

        j.CodModalidade = DemonstrativoConsolidacao.Modalidade.cdMod;
        j.DataEvento = "00000000";
        j.HoraEvento = "0000000";
        j.TipoConsulta = 2;
        p.act = 'RecuperarDebitosDemonstrativoArt39e40';
        p.dt = JSON.stringify(j);
        DAL.posting('AjajAdesaoManual.aspx', p, DemonstrativoConsolidacao.tratar_debitos_retornados, 'm');

    },

    //Método para tratar os dados do DTO retornado da consulta de débitos
    tratar_debitos_retornados: function () {
        var cr = DAL.cr.m, dados_retornados = DAL.data.m, msg;
        delete DAL.cr.m;
        delete DAL.data.m;

        if (cr !== '00') {
            $('#msgerr').text(cr + ' - ' + dt);
            exibirErro(MSG.E1, MSG.btn);
        } else {
            if ($.type(dados_retornados.retorno.CodRetorno) !== "undefined") {
                if (dados_retornados.retorno.CodRetorno === "0000") {
                    var debitos = dados_retornados.debitos;
                    var debitosDTO = new Array();

                    for (var j = 0; j < debitos.length; j++) {// debitos por ni e processo

                        var debito_atual = debitos[j];
                        var processos = debito_atual.processos;
                        var ni = debito_atual.Ni;

                        for (var chave in processos) {// cada processo tem um ou mais débitos 

                            var quadro = new Array(); // quadro é cada tabela que será montada na tela
                            var nr_processo = chave;
                            var dados_processo = processos[chave]; // processo atual
                            // quadro recebe ni e processo
                            var debitos_consolidados = new Array();

                            for (var k = 0; k < dados_processo.length; k++) {

                                var debito_atual = new Array();
                                debito_atual.nr_processo = dados_processo[k].Processo;
                                debito_atual.codigo_da_receita = dados_processo[k].CodigoReceita;
                                debito_atual.periodicidade = dados_processo[k].Periodicidade;
                                debito_atual.periodo_de_apuracao = dados_processo[k].PeriodoApuracao;
                                debito_atual.data_de_vencimento = dados_processo[k].DataVencimento;
                                debito_atual.valor_principal = dados_processo[k].ValorPrincipal;
                                debito_atual.valor_da_multa = dados_processo[k].ValorMulta;
                                debito_atual.valor_dos_juros = dados_processo[k].ValorJuros;
                                debito_atual.valor_consolidado_sem_reducoes = dados_processo[k].ValorSemReducoes;
                                debito_atual.numero_inscricao = dados_processo[k].NumeroInscricao;
                                debito_atual.valor_encargos = dados_processo[k].Encargos;

                                debitos_consolidados.push(debito_atual);

                            }
                            quadro.push(ni);
                            quadro.push(nr_processo);
                            quadro.push(debitos_consolidados);

                            debitosDTO.push(quadro);
                        }
                    }
                    if (DemonstrativoConsolidacao.isProcesso()) {
                        DemonstrativoConsolidacao.listar_processos(debitosDTO);
                    }
                    if (DemonstrativoConsolidacao.isInscricao()) {
                        DemonstrativoConsolidacao.listar_inscricoes(debitosDTO);
                    }
                } else {
                    $('#msgerr').text(dados_retornados.retorno.CodRetorno + ' - ' + dados_retornados.retorno.MsgErroRet);
                    MSG.tratar_mensagens(dados_retornados.retorno.CodRetorno, dados_retornados.retorno.MsgErroRet);
                }
            } else {
                $('#msgerr').text("erro desconhecido");
                exibirErro(MSG.E1, MSG.btnRetornar);
            }
        }
    },

    //metodo que monta o quadro dos débitos retornados  

    listar_processos: function (dto) {
        var processo_demonstrativo = $('#debitos_demonstrativo');
        var listaNiExibido = [];
        var existeNi = function (elemento) {
            return listaNiExibido.indexOf(elemento) >= 0;
        };

        for (var j = 0, _len_j = dto.length; j < _len_j; j++) {
            if (!existeNi(dto[j][0])) {
                var strHtmlNi = '';
                strHtmlNi += '<div class="line"><div class="unit unitLabel line lineform"><label>CNPJ:</label><span id="' + dto[j][0] + '">' + dto[j][0] + '</span></div></div>';
                processo_demonstrativo.append(strHtmlNi);
                listaNiExibido.push(dto[j][0]);
            }
            // incluir processo
            var strHtmlProcesso = '';
            strHtmlProcesso += '<div class="line"><div class="unit unitLabel line lineform"><label>Processo n&ordm;:</label>';
            strHtmlProcesso += '<span id="' + dto[j][1] + '">' + this.getNuProcessoFormatado(dto[j][1]) + '</span></div></div>';
            processo_demonstrativo.append(strHtmlProcesso);

            var htmlTabela = '';
            htmlTabela += '<table class="dr-table dataGrid tblPrincipal"><thead><tr class="dr-table-header">';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Código da Receita</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Período de Apuração</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Data de Vencimento</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor Principal <br /> R$</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor da Multa <br />  R$</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor dos Juros <br />  R$</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor Consolidado <br /> sem Reduções <br /> R$</th></tr></thead><tbody>';

            for (var i = 0, $linha = '', o_debito = [], _len_i = dto[j][2].length; i < _len_i; i++) {
                o_debito = dto[j][2][i];
                var lineClass = (i % 2 == 0) ? 'lineEven' : 'lineOdd';

                htmlTabela += '<tr class="' + lineClass + '">';
                htmlTabela += '<td class="dr-table-bodycell center">' + o_debito.codigo_da_receita + '</td>';
                htmlTabela += '<td class="dr-table-bodycell center">' + DemonstrativoConsolidacao.getPeriodoDeApuracaoFormatado(o_debito.periodicidade, o_debito.periodo_de_apuracao) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell center">' + Utils.AAAAMMDDToDDMMAAAA(o_debito.data_de_vencimento, "true") + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_principal)) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_da_multa)) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_dos_juros)) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_consolidado_sem_reducoes)) + '</td>';
                htmlTabela += '</tr>';
            }
            htmlTabela += '</tbody></table>';
            processo_demonstrativo.append(htmlTabela);
        }
        DemonstrativoConsolidacao.gerar_divida_consolidada();

    },


    listar_inscricoes: function (dto) {
        var processo_demonstrativo = $('#inscricao_demonstrativo');
        var listaNiExibido = [];
        var existeNi = function (elemento) {
            return listaNiExibido.indexOf(elemento) >= 0;
        };

        for (var j = 0, _len_j = dto.length; j < _len_j; j++) {
            if (!existeNi(dto[j][0])) {
                var strHtmlNi = '';
                strHtmlNi += '<div class="line"><div class="unit unitLabel line lineform"><label>CNPJ:</label><span id="' + dto[j][0] + '">' + dto[j][0] + '</span></div></div>';
                processo_demonstrativo.append(strHtmlNi);
                listaNiExibido.push(dto[j][0]);
            }
            // incluir processo
            var strHtmlProcesso = '';
            strHtmlProcesso += '<div class="line"><div class="unit unitLabel line lineform"><label>Processo n&ordm;:</label>';
            strHtmlProcesso += '<span id="' + dto[j][1] + '">' + this.getNuProcessoFormatado(dto[j][1]) + '</span></div></div>';
            processo_demonstrativo.append(strHtmlProcesso);

            var htmlTabela = '';
            htmlTabela += '<table class="dr-table dataGrid tblPrincipal"><thead><tr class="dr-table-header">';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Código da Receita</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Número da Inscrição</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor Principal <br /> R$</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor da Multa <br />  R$</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor dos Juros <br />  R$</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Encargos R$</th>';
            htmlTabela += '<th class="dr-table-headercell center col-width-padrao">Valor Consolidado <br /> sem Reduções <br /> R$</th></tr></thead><tbody>';

            for (var i = 0, $linha = '', o_debito = [], _len_i = dto[j][2].length; i < _len_i; i++) {
                o_debito = dto[j][2][i];
                var lineClass = (i % 2 == 0) ? 'lineEven' : 'lineOdd';

                htmlTabela += '<tr class="' + lineClass + '">';
                htmlTabela += '<td class="dr-table-bodycell center">' + o_debito.codigo_da_receita + '</td>';
                htmlTabela += '<td class="dr-table-bodycell center">' + Utils.getNuInscricaoFormatado(o_debito.numero_inscricao) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_principal)) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_da_multa)) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_dos_juros)) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_encargos)) + '</td>';
                htmlTabela += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Number(o_debito.valor_consolidado_sem_reducoes)) + '</td>';
                htmlTabela += '</tr>';
            }
            htmlTabela += '</tbody></table>';
            processo_demonstrativo.append(htmlTabela);
        }
        DemonstrativoConsolidacao.gerar_divida_consolidada();
    },


    gerar_divida_consolidada: function () {

        var j = {}, p = {};
        j.TipoNi = DemonstrativoConsolidacao.NI.tp;
        j.NI = DemonstrativoConsolidacao.NI.ni;

        j.CodModalidade = DemonstrativoConsolidacao.Modalidade.cdMod;
        j.DataEvento = "00000000";
        j.HoraEvento = "0000000";
        j.TipoConsulta = 3;
        p.act = 'RecuperarDividaConsolidadaDemonstrativoArt39e40';
        p.dt = JSON.stringify(j);
        DAL.posting('AjajAdesaoManual.aspx', p, DemonstrativoConsolidacao.exibir_divida_consolidada, 'm');
    },



    exibir_divida_consolidada: function () {
        var cr = DAL.cr.m, dados_retornados = DAL.data.m, msg;
        delete DAL.cr.m;
        delete DAL.data.m;
        if (cr !== '00') {
            $('#msgerr').text(cr + ' - ' + dt);
            exibirErro(MSG.E1, MSG.btn);
        } else {
            if ($.type(dados_retornados.retorno.CodRetorno) !== "undefined") {
                if (dados_retornados.retorno.CodRetorno === "0000") {

                    var dados = dados_retornados.divida_consolidada;

                    var quadro_divida_consolidada = $('#quadro_divida_consolidada');
                    var string_html = '';
                    string_html += '<legend class="legend">Dívida Consolidada</legend>';
                    string_html += '<table class="dr-table dataGrid tblPrincipal">';
                    string_html += '<thead>';
                    string_html += '<tr class="dr-table-header">';
                    string_html += '<th class="dr-table-headercell center col-width-padrao"></th>';
                    string_html += '<th class="dr-table-headercell center col-width-padrao">Principal</th>';
                    string_html += '<th class="dr-table-headercell center col-width-padrao">Multa Isolada</th>';
                    string_html += '<th class="dr-table-headercell center col-width-padrao">Multas de Mora/Ofício</th>';
                    string_html += '<th class="dr-table-headercell center col-width-padrao">Juros de Mora</th>';
                    if (DemonstrativoConsolidacao.isInscricao()) {
                        string_html_divida_consolidada += '<th class="dr-table-headercell center col-width-padrao">Encargos</th>';
                    }
                    string_html += '<th class="dr-table-headercell center col-width-padrao">Total</th>';
                    string_html += '</tr>';
                    string_html += '</thead>';
                    string_html += '<tbody>';
                    string_html += '<tr class="lineOdd">';
                    string_html += '<td class="dr-table-bodycell left">Valores sem Redu&ccedil;&atilde;o</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[0].ValorPrincipal))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[0].ValorMultaIsolada))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[0].ValorMultaMora))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[0].ValorJuros))) + '</td>';
                    if (DemonstrativoConsolidacao.isInscricao()) {
                        string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[0].ValorEncargos))) + '</td>';
                    }
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[0].ValorTotal))) + '</td>';
                    string_html += '</tr>';

                    string_html += '<tr class="lineEven">';
                    string_html += '<td class="dr-table-bodycell left">Valores das Redu&ccedil;&otilde;es</td>';
                    string_html += '<td class="dr-table-bodycell right"></td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[1].ValorMultaIsolada))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[1].ValorMultaMora))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[1].ValorJuros))) + '</td>';
                    if (DemonstrativoConsolidacao.isInscricao()) {
                        string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[1][4]))) + '</td>';
                    }
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[1].ValorTotal))) + '</td>';
                    string_html += '</tr>';
                    string_html += '</tbody>';

                    string_html += '<tfoot>';
                    string_html += '<tr class="tableFooter">';
                    string_html += '<td class="dr-table-bodycell left">Valores com Redu&ccedil;&atilde;o</td>';
                    string_html += '<td class="dr-table-bodycell right" id="vlPrincReduzido">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[2].ValorPrincipal))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[2].ValorMultaIsolada))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[2].ValorMultaMora))) + '</td>';
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[2].ValorJuros))) + '</td>';
                    if (DemonstrativoConsolidacao.isInscricao()) {
                        string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[2].ValorEncargos))) + '</td>';
                    }
                    string_html += '<td class="dr-table-bodycell right">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[2].ValorTotal))) + '</td>';
                    string_html += '</tr>';
                    string_html += '<tr class="tableFooter">';
                    string_html += '<td colspan="5">VALOR DA DÍVIDA COM REDUÇÃO:</td>';
                    string_html += '<td class="dr-table-bodycell right" id="totalDividaRFB">' + Utils.formataNumeros(Utils.divideporcem(Number(dados[2].ValorTotal))) + '</td>';
                    string_html += '</tr>';
                    string_html += '</tfoot>';

                    quadro_divida_consolidada.append(string_html);
                    DemonstrativoConsolidacao.gerePrejuizoFiscal_BCN_CSLL();
                } else {
                    $('#msgerr').text(dados_retornados.retorno.CodRetorno + ' - ' + dados_retornados.retorno.MsgErroRet);
                    MSG.tratar_mensagens(dados_retornados.retorno.CodRetorno, dados_retornados.retorno.MsgErroRet);
                }
            } else {
                $('#msgerr').text("erro desconhecido");
                exibirErro(MSG.E1, MSG.btnRetornar);
            }
        }
    },

    gereDadosPrestacao: function () {
        var codModalidade = DemonstrativoConsolidacao.Modalidade.cdMod;
        var mod = parseInt(codModalidade, 10);
        if (mod >= 961 && mod <= 964) {
            var j = {}, p = {};
            j.TipoNi = DemonstrativoConsolidacao.NI.tp;
            j.Ni = DemonstrativoConsolidacao.NI.ni;
            j.CodModalidade = codModalidade;
            j.TipoConsulta = '5';
            p.act = 'ObterInformacoesPrestacaoLei12865';
            p.dt = JSON.stringify(j);
            DAL.posting('AjajAdesaoManual.aspx', p, DemonstrativoConsolidacao.listeDadosPrestacao, 'm');
        }
        else {
            $('#prestacao').hide();
        }
    },

    listeDadosPrestacao: function () {
        var cr = DAL.cr.m, dtoRetorno = DAL.data.m, msg, cdRet;
        if (cr != '00') {
            $('#msgerr').text(cr + ' - ' + dt);
            exibirErro($('#msgerr').text(), MSG.btnErroRetornar);
        }
        else {
            if ($.type(dtoRetorno.CodRetorno) != "undefined") {
                exibirErro(dtoRetorno.MsgErroRet, MSG.btnErroRetornar);
            }
            else {
                cdRet = parseInt(dtoRetorno.RetornoVO.CodRetorno, 10);
                if (cdRet == 0) {
                    $('#prestacao').show();
                    $('#vl_1a_prestacao').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.InformacoesPrestacao.ValorPrimeiraPrestacao))));
                    $('#vl_prestacoes_remanescentes').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.InformacoesPrestacao.ValorPrestacao))));

                    var prestacoes = parseInt(dtoRetorno.InformacoesPrestacao.Quantidade, 10);
                    $('#qtde_prestacoes').text(prestacoes);
                    $('#textoQtdePrestacoesRemanescentes').text('Valor básico das ' + (prestacoes - 1) + ' prestações remanescentes');
                    DemonstrativoConsolidacao.MontarTela2();
                }
                else { //ocorreu algum erro ao tentar recuperar informações do GP.
                    exibirErro(dtoRetorno.RetornoVO.MsgErroRet, MSG.btnErroRetornar);
                }
            }
        }
    },

    gerePrejuizoFiscal_BCN_CSLL: function () {
        var codModalidade = DemonstrativoConsolidacao.Modalidade.cdMod;
        if (codModalidade == '963' || codModalidade == '964' || codModalidade == '967' || codModalidade == '968') {
            var j = {}, p = {};
            j.TipoNi = DemonstrativoConsolidacao.NI.tp;
            j.Ni = DemonstrativoConsolidacao.NI.ni;
            j.CodModalidade = codModalidade;
            j.TipoConsulta = '4';
            p.act = 'ObterQuadroPFBCNLei12865';
            p.dt = JSON.stringify(j);
            DAL.posting('AjajAdesaoManual.aspx', p, DemonstrativoConsolidacao.callbackObterQuadroPFBCN, 'm');
        }
        else {
            $('#prejuizo_fiscal').hide();
        }
        DemonstrativoConsolidacao.gereDadosPrestacao();
    },

    callbackObterQuadroPFBCN: function () {
        var cr = DAL.cr.m, dtoRetorno = DAL.data.m, msg, cdRet;
        if (cr != '00') {
            $('#msgerr').text(cr + ' - ' + dt);
            exibirErro($('#msgerr').text(), MSG.btnErroRetornar);
        } else {
            //console.log('Verificando possivel erro : ' + dtoRetorno.CodRetorno);
            if ($.type(dtoRetorno.CodRetorno) != "undefined") {
                exibirErro(dtoRetorno.MsgErroRet, MSG.btnErroRetornar);
            }
            else {
                cdRet = parseInt(dtoRetorno.RetornoVO.CodRetorno, 10);
                if (cdRet == 0) {
                    $('#prejuizo_fiscal').show();
                    //console.log('Resultado OK ' + dtoRetorno);
                    //setando as informações no quadro de PF/BCN . . .
                    //setando dados de limites definidos para amortização
                    $('#pf_bcn_principal').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.LimiteValoresAmortizar.ValorPrincipal))));
                    $('#pf_bcn_multa').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.LimiteValoresAmortizar.ValorMulta))));
                    $('#pf_bcn_juros').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.LimiteValoresAmortizar.ValorJuros))));
                    $('#pf_bcn_total').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.LimiteValoresAmortizar.ValorTotal))));
                    //inserindo linhas no corpo da tabela . . .
                    var i;
                    for (i = 0; i < dtoRetorno.QuadroInformacoes.InformacoesPFBCN.length; i++) {

                        var informacaoPFBCN = dtoRetorno.QuadroInformacoes.InformacoesPFBCN[i];

                        var lineClass = (i % 2 == 0) ? 'lineOdd' : 'lineEven';

                        //construindo as linhas de informações. Começando com prejuízo fiscal
                        var linhas = '<tr style="height:30px" class="' + lineClass + '">';
                        var colunasLinhas = '<td class="dr-table-cell center" rowspan="2">' + Utils.getCNPJFormatado(informacaoPFBCN.Cnpj) + '</td>';
                        if (informacaoPFBCN.PrejuizoFiscal != 'undefined' && informacaoPFBCN.PrejuizoFiscal != null) {
                            colunasLinhas += DemonstrativoConsolidacao.preencherLinhaPfBcnComDados(informacaoPFBCN.PrejuizoFiscal, '1');
                        }
                        else {
                            colunasLinhas += DemonstrativoConsolidacao.preencherLinhaPfBcnComZeros('Prejuízo Fiscal', '25');
                        }
                        linhas += colunasLinhas + '</tr>';

                        //inserindo informações de base de cálculo . . .
                        linhas += '<tr class="' + lineClass + '">';
                        if (informacaoPFBCN.BaseCalculo != 'undefined' && informacaoPFBCN.BaseCalculo != null) {
                            colunasLinhas = DemonstrativoConsolidacao.preencherLinhaPfBcnComDados(informacaoPFBCN.BaseCalculo, '2');
                        }
                        else {
                            colunasLinhas = DemonstrativoConsolidacao.preencherLinhaPfBcnComZeros('Base de Cálculo<br/>Negativa de CSLL', '9');
                        }
                        linhas += colunasLinhas + '</tr>';

                        $('#prejuizo_fiscal_tbody').append(linhas);
                    }
                    //setando as informações do rodapé . . . 
                    //valores totais amortizados
                    $('#vl_amortizado_pf_bcn_principal').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.ValorTotalAmortizado.ValorPrincipal))));
                    $('#vl_amortizado_pf_bcn_multa').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.ValorTotalAmortizado.ValorMulta))));
                    $('#vl_amortizado_pf_bcn_juros').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.ValorTotalAmortizado.ValorJuros))));
                    $('#vl_amortizado_pf_bcn_total').text(Utils.formataNumeros(Utils.divideporcem(parseFloat(dtoRetorno.QuadroInformacoes.ValorTotalAmortizado.ValorTotal))));

                    //valores remanescentes
                    $('#vlPrincReduzido').text();

                    //valor principal remanescente : valor principal com reduções - valor principal amortizado;
                    var vlPrincRemanescente; // = new BigNumber('0');
                    //recuperação de valores do quadro de divida consolidada comprometida . . . não arriscar quebrar toda a tela.
                    try {
                        vlPrincRemanescente = new BigNumber(Utils.converterMoedaEmDecimal($('#vlPrincReduzido').text()));
                    }
                    catch (err) {
                        vlPrincRemanescente = new BigNumber('0');
                    }
                    vlPrincRemanescente = vlPrincRemanescente.minus(Utils.converterMoedaEmDecimal($('#vl_amortizado_pf_bcn_principal').text()));

                    var vlMultaRemanescente = new BigNumber(Utils.converterMoedaEmDecimal($('#pf_bcn_multa').text()));
                    vlMultaRemanescente = vlMultaRemanescente.minus(Utils.converterMoedaEmDecimal($('#vl_amortizado_pf_bcn_multa').text()));

                    var vlJurosRemanescente = new BigNumber(Utils.converterMoedaEmDecimal($('#pf_bcn_juros').text()));
                    vlJurosRemanescente = vlJurosRemanescente.minus(Utils.converterMoedaEmDecimal($('#vl_amortizado_pf_bcn_juros').text()));

                    var vlTotalRemanescente = vlPrincRemanescente.plus(vlMultaRemanescente).plus(vlJurosRemanescente);

                    $('#vl_remanescente_pf_bcn_principal').text(Utils.converterDecimalEmMoeda(vlPrincRemanescente.toString()));
                    $('#vl_remanescente_pf_bcn_multa').text(Utils.converterDecimalEmMoeda(vlMultaRemanescente.toString()));
                    $('#vl_remanescente_pf_bcn_juros').text(Utils.converterDecimalEmMoeda(vlJurosRemanescente.toString()));
                    $('#vl_remanescente_pf_bcn_total').text(Utils.converterDecimalEmMoeda(vlTotalRemanescente.toString()));

                    //valor da divida apos amortização
                    $('#vl_total_apos_amortizacao').text(Utils.converterDecimalEmMoeda(vlTotalRemanescente.toString()));

                }
                else if (cdRet == 41) { //pesquisa realizada não recuperou nenhuma informação
                    $('#prejuizo_fiscal').hide();
                }
                else { //Ocorreu algum erro durante o processamento . . .
                    exibirErro(dtoRetorno.RetornoVO.MsgErroRet, MSG.btnErroRetornar);
                }
            }
        }
    },

    preencherLinhaPfBcnComDados: function (creditoPfBcnEN, tipoCredito) {
        var percentual = (tipoCredito == '1') ? '25' : '9';

        tipoCredito = (tipoCredito == '1') ? 'Prejuízo Fiscal' : 'Base de Cálculo<br/>Negativa de CSLL';

        var align = ['left', 'right', 'center', 'right', 'right', 'right', 'right', 'right'];

        var colunasLinha = '<td class="dr-table-cell ' + align[0] + '"' + '>' + tipoCredito + '</td>';

        colunasLinha += '<td class="dr-table-cell ' + align[1] + '"' + '>' +
        Utils.formataNumeros(Utils.divideporcem(parseFloat(creditoPfBcnEN.MontanteUtilizado))) + '</td>';

        colunasLinha += '<td class="dr-table-cell ' + align[2] + '"' + '>' + creditoPfBcnEN.Percentual + '</td>';

        colunasLinha += '<td class="dr-table-cell ' + align[3] + '"' + '>' +
        Utils.formataNumeros(Utils.divideporcem(parseFloat(creditoPfBcnEN.ValorCredito))) + '</td>';

        colunasLinha += '<td class="dr-table-cell ' + align[4] + '"' + '>' +
        Utils.formataNumeros(Utils.divideporcem(parseFloat(creditoPfBcnEN.ValoresAmortizacao.ValorPrincipal))) + '</td>';

        colunasLinha += '<td class="dr-table-cell ' + align[5] + '"' + '>' +
        Utils.formataNumeros(Utils.divideporcem(parseFloat(creditoPfBcnEN.ValoresAmortizacao.ValorMulta))) + '</td>';

        colunasLinha += '<td class="dr-table-cell ' + align[6] + '"' + '>' +
        Utils.formataNumeros(Utils.divideporcem(parseFloat(creditoPfBcnEN.ValoresAmortizacao.ValorJuros))) + '</td>';

        colunasLinha += '<td class="dr-table-cell ' + align[7] + '"' + '>' +
        Utils.formataNumeros(Utils.divideporcem(parseFloat(creditoPfBcnEN.ValoresAmortizacao.ValorTotal))) + '</td>';

        return colunasLinha;
    },

    preencherLinhaPfBcnComZeros: function (tipoCredito, percentual) {
        var align = ['left', 'right', 'center', 'right', 'right', 'right', 'right', 'right'];
        var i;
        var colunasLinha = '';
        for (i = 0; i < align.length; i++) {
            colunasLinha += '<td class="dr-table-cell ' + align[i] + '"' + '>';
            if (i == 0) {
                colunasLinha += tipoCredito;
            }
            else if (i == 2) {
                colunasLinha += percentual;
            }
            else {
                colunasLinha += '0,00';
            }
            colunasLinha += '</td>';
        }
        return colunasLinha;
    },
    limparDadosTabelaPFBCN: function () {
        //limpando informações no header - limite a amortizar . . .
        $('#pf_bcn_principal').empty();
        $('#pf_bcn_multa').empty();
        $('#pf_bcn_juros').empty();
        $('#pf_bcn_total').empty();
        //limpando informações de linhas de informações
        $('#prejuizo_fiscal_tbody').empty();
        //limpando informações de rodapé - valores amortizados
        $('#vl_amortizado_pf_bcn_principal').empty();
        $('#vl_amortizado_pf_bcn_multa').empty();
        $('#vl_amortizado_pf_bcn_juros').empty();
        $('#vl_amortizado_pf_bcn_total').empty();
        //limpando informações de rodapé - valores remanescentes
        $('#vl_remanescente_pf_bcn_principal').empty();
        $('#vl_remanescente_pf_bcn_multa').empty();
        $('#vl_remanescente_pf_bcn_juros').empty();
        $('#vl_remanescente_pf_bcn_total').empty();
        //limpando o valor total após amortização
        $('#vl_total_apos_amortizacao').empty();
    },
    limparDadosPrestacoes: function () {
        $('#qtde_prestacoes').empty();
        $('#vl_1a_prestacao').empty();
        $('#textoQtdePrestacoesRemanescentes').empty();
        $('#vl_prestacoes_remanescentes').empty();
    },
    getPeriodoDeApuracaoFormatado: function (co_tipo_periodo, da_pa_original) {
        // WWW - Primeiro elemento indice ZERO, artificio para tornar janeiro == 01
        var meses = 'WWWJANFEVMARABRMAIJUNJULAGOSETOUTNOVDEZ'.match(/[A-Z]{3}/gm),
            r = da_pa_original.match(/(\d{4})(\d{2})(\d{2})/);
        if (r == null) return da_pa_original;
        var aaaa = r[1], mm = r[2], dd = r[3];
        //
        var tmp = '';
        switch (co_tipo_periodo) {
            case 'AN': // anual
                if (dd === '01' && mm === '01')
                    return aaaa;
                break;
            /*
            // nao definido na pauta
            case 'SE': // semestral
            return dd + '/' + meses[ parseInt(mm) ]+ '/' + aaaa;
            break;
            */ 
            case 'TR': // Trimestral
                var trimestre = { '01': '1', '04': '2', '07': '3', '10': '4' };
                if (trimestre[mm])
                    return trimestre[mm] + '&ordm; T' + '/' + aaaa;
                break;
            case 'ME': // Mensal
                if (dd === '01')
                    return meses[parseInt(mm)] + '/' + aaaa;
                break;
            case 'QZ': // quinzenal
                if (dd === '01')
                    return '1&ordf; Q/' + meses[parseInt(mm)] + '/' + aaaa;
                if (dd === '16')
                    return '2&ordf; Q/' + meses[parseInt(mm)] + '/' + aaaa;
                break;
            case 'DC': // decendial
                var decendio = { '01': '1', '11': '2', '21': '3' };
                if (decendio[dd])
                    return decendio[dd] + '&ordm; D/' + meses[parseInt(mm)] + '/' + aaaa;
                break;
            case 'SM': // semanal
                if (/^0[1-6]$/.test(dd))
                    return '' + parseInt(dd) + '&ordf; S/' + meses[parseInt(mm)] + '/' + aaaa;
                break;
            case 'DI': // diario
                return dd + '/' + meses[parseInt(mm)] + '/' + aaaa;
        }
        return '?' + dd + '/' + meses[parseInt(mm)] + '/' + aaaa;
    },
    getNuProcessoFormatado: function (proc) {
        proc = Utils.converterValor(proc, 's');
        if (proc.length != 15 && proc.length != 17 && proc.length != 20) {
            return false;
        }

        var procRegex = new RegExp('^([0-9]{5})([0-9]{3})([0-9]{3})([0-9]{2}|[0-9]{4})([0-9]{2})$');
        return proc.replace(procRegex, '$1.$2.$3/$4-$5');

    }
};

$(function () {
    DemonstrativoConsolidacao.Iniciar();
});

$(function () {

    // funcao chamadora do plugin dom2pdf
    $('input[class="icon pdf"]').click(function () {
        //Para carregar primeiro os débitos antes de gerar o PDF.
        var interval = setInterval(function () {
            clearInterval(interval);

            // o browser calcula a altura e largura do DIV. "auto" é o valor default. Sem isso, só mostra o primeiro quadro
            $("#mainBodyContainer").height("auto").width("auto");
           // $("table").css("page-break-before", "auto");
            // $("table").css("page-break-inside","avoid");
             $("div.unit unitLabel line lineform").css("page-break-inside","avoid");
            
            
            // tira o cabeçalho com o titulo da Lei e botoes de impressa
            $("#bodyHeader").css("display", "none");
            $(".navigationButtons").hide();

            $("div.liquid,body").css("backgroundColor", "#ffffff");

            $("#prejuizo_fiscal").css("font-size", "10px");

            $('html').dom2Pdf();

            $("#bodyHeader").css("display", "block");
            $("#prejuizo_fiscal").css("font-size", "12px");
            $(".navigationButtons").show();

            window.console.log('Aguardando geração do PDF...', new Date());
        }, 500);
    });
});

