$(function() {
    $("#sum").click(somar);
});
 
function somar() {
   // event.preventDefault();
 
    $(".required").removeClass("required");
    $("span").empty();
    
    var data = {
        a : $("#a").val(),
        b : $("#b").val()
    };
    Calculator.somar(data).done(sumOk).fail(Calculator.sumFailed);
};

function sumOk(data) {
    $("#result").val(data);
}

