package Model;

import java.util.Date;

/**
 * Created by 09534485845 on 28/01/2016.
 */
public class Nota {

    private String id;
    private String nome;
    private String texto;
    private String tipo;
    private String data;
    private String url;

    public Nota(){}

    public Nota(String id, String nome, String texto, String tipo, String data, String url) {
        this.id = id;
        this.nome = nome;
        this.texto = texto;
        this.tipo = tipo;
        this.data = data;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public String toString() {
        return "Dominio.Nota{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", texto='" + texto + '\'' +
                ", tipo=" + tipo +
                ", data=" + data +
                ", url='" + url + '\'' +
                '}';
    }
}
