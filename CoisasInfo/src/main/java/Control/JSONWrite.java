package Control; /**
 * Created by 09534485845 on 28/01/2016.
 */


import java.io.*;

import Dominio.Nota;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONWrite {

    public static void gravar(Nota nota) {
        JSONObject jsonObject = new JSONObject();

        FileWriter writeFile = null;

        //Armazena dados em um Objeto JSON
        jsonObject.put("nome", nota.getNome());
        jsonObject.put("texto", nota.getTexto());
        jsonObject.put("tipo", nota.getTipo());
        jsonObject.put("data", nota.getData());
        jsonObject.put("url", nota.getUrl());

        try {


            writeFile = new FileWriter("/home/09534485845/nota" + nota.getId()+".txt");
            //Escreve no arquivo conteudo do Objeto JSON
            writeFile.write(jsonObject.toJSONString());
            writeFile.close();


            System.out.println(nota.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }    //Imprimne na Tela o Objeto JSON para vizualização
    // System.out.println(jsonObject);

    //Leia mais em: Leitura e Escrita de arquivos JSON em Java http://www.devmedia.com.br/leitura-e-escrita-de-arquivos-json-em-java/27663#ixzz3yZ47ZWi6

    public static JSONObject ler(Nota nota) {
        JSONObject jsonObject = null;
        JSONParser parser = new JSONParser();
        try {
            //Salva no objeto JSONObject o que o parse tratou do arquivo
            jsonObject = (JSONObject) parser.parse(new FileReader("nota" + nota.getId()));

            //Salva nas variaveis os dados retirados do arquivo
            // nome = (String) jsonObject.get("nome");
            //   sobrenome = (String) jsonObject.get("sobrenome");
            //  estado = (String) jsonObject.get("estado");
            //  pais = (String) jsonObject.get("pais");

            // System.out.printf( "Nome: %s\nSobrenome: %s\nEstado: %s\nPais: %s\n",  nome, sobrenome, estado, pais);
        }
        //Trata as exceptions que podem ser lançadas no decorrer do processo
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }
}

//Leia mais em: Leitura e Escrita de arquivos JSON em Java http://www.devmedia.com.br/leitura-e-escrita-de-arquivos-json-em-java/27663#ixzz3yZ6hclo8



