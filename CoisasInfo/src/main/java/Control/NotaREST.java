package Control;

import CacheDao.CacheJSON;
import CacheDao.NotaDAO;
import Dominio.Nota;

import javax.ws.rs.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

@Path("nota")
public class NotaREST {

    private CacheJSON cacheJSON= null;

    public NotaREST(){
        cacheJSON = new  CacheJSON();
    }

    @POST
    @Path("inserir")
    @Consumes("application/json")
    @Produces("text/plain")
    public String inserir(Nota nota) {
        try {
           // if (nota.getId() != null) {
                //cacheJSON.atualizar(nota);
           // } else {
                //cacheJSON.gravar(nota);
            //}
            NotaDAO notadao= new NotaDAO();
            notadao.Save(nota);

        }catch (Exception e){

        }
        return "Sucesso";
    }
    
    
    @GET
    @Path("{id}")
    @Produces("application/json")
    public Nota obter(@PathParam("id") Long id)  {
       // Nota nota = cacheJSON.ler("src/main/arquivos/nota1.txt") ;
        NotaDAO notadao= new NotaDAO();
        Nota nota = null;
        try {
            nota = notadao.getNotaById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nota;
    }
    
    @GET
    @Produces("application/json")
    public List<Nota> buscar() {
        System.out.print("PASSOU em NotaREST.buscar getNotas");
        //Jedis jedis = new Jedis("localhost");
        //// String resultado = jedis.get("test");
        //System.out.print(resultado);

        NotaDAO notadao= new NotaDAO();

        ArrayList<Nota> listaNotas = new ArrayList<Nota>() ;

        try {
            listaNotas = notadao.getNotas();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  listaNotas;
    }

}// fim da classe
