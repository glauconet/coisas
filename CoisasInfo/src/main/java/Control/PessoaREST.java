package Control;

import Model.Pessoa;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


@Path("pessoa")
public class PessoaREST {
   
    
    
    @POST
    @Path("inserir")
    @Consumes("application/json")
    @Produces("text/plain")
    public String inserir(Pessoa pessoa) {
        System.out.println(pessoa.getNome() + ", " + pessoa.getEmail());
        Integer id = 1;
        String url = "http://localhost:8080/cadastro/api/pessoas/" + id;
        return url;
    }
    
    
    @GET
    @Path("{id}")
    @Produces("application/json")
    public Pessoa obter(@PathParam("id") Integer id) throws Exception {
        Pessoa pessoa = new Pessoa();
 
       pessoa.setNome("teste");
       pessoa.setEmail("testeemail");
       pessoa.setTelefone("testetel");
          
 
        return pessoa;
    }
    
    @GET//   @Path("listar")
    @Produces("application/json")
    public List<Pessoa> buscar() {
        List<Pessoa> result = new ArrayList<Pessoa>();
        
       Pessoa pessoa1 = new Pessoa();
       pessoa1.setId(10);
        pessoa1.setNome("John Malkovich");
        pessoa1.setEmail("john.malkovich@gmail.com");
        pessoa1.setTelefone("(71) 1234-5678");
        result.add(pessoa1);
 
        Pessoa pessoa2 = new Pessoa();
        pessoa2.setId(20);
        pessoa2.setNome("Cleverson Sacramento");
        pessoa2.setEmail("cleverson.sacramento@serpro.gov.br");
        pessoa2.setTelefone("(71) 5555-6666");
        result.add(pessoa2);
 
        Pessoa pessoa3 = new Pessoa();
        pessoa3.setId(30);
        pessoa3.setNome("Luciano Borges");
        pessoa3.setEmail("luciano.borges@serpro.gov.br");
        pessoa3.setTelefone("(71) 0000-9999");
       
        result.add(pessoa3);
        
        
 
        return result.isEmpty() ? null : result;
    }
    /*public static class Pessoa {
        
      public String nome ;
      public String email ;
      public String telefone ;
     
    }*/
   
}
