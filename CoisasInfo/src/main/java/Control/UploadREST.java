package Control;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ws.rs.Path;

/**
 * Created by 09534485845 on 04/02/2016.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.*;
import javax.ws.rs.core.*;


@Path("file")
public class UploadREST {

    protected  String UPLOADED_FILE_PATH = null;

    public UploadREST(){
         if(System.getProperty("os.name").equals("Linux")){
                    UPLOADED_FILE_PATH = "/home/09534485845/afiles/gitprojects/coisas/CoisasInfo/src/main/arquivos/";
         }else{
                    UPLOADED_FILE_PATH = "D:\\03-Projetos\\CoisasProject\\CoisasInfo\\CoisasInfo\\src\\main\\arquivos\\";
         }
    }



    @POST
    @Path("upload")
    @Consumes("multipart/form-data")
    public Response uploadFile(MultipartFormDataInput dataInput) {

        String fileName = "";
        Response response = null;
        System.out.print("chegou em upload>>>>>>>>>>>>>>>>>>>>>>>>>");
        Map<String, List<InputPart>> uploadForm = dataInput.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("nameOfInputTypeFileOnHtmlForm");

        for (InputPart inputPart : inputParts) {
            try {
                MultivaluedMap<String, String> header = inputPart.getHeaders();
                fileName = getFileName(header);

                InputStream inputStream = inputPart.getBody(InputStream.class, null);

                byte[] bytes = IOUtils.toByteArray(inputStream);

                writeFile(bytes, UPLOADED_FILE_PATH + fileName);

                response = Response.status(201).entity(fileName).build();

            } catch (IOException e) {
                e.printStackTrace();
                response = Response.status(400).build();
            }
        }
        return response;
    }

    private String getFileName(MultivaluedMap<String, String> header) {

        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

        for (String filename : contentDisposition) {
            if ((filename.trim().startsWith("filename"))) {
                String[] name = filename.split("=");
                String finalFileName = name[1].trim().replaceAll("\"", "");
                return finalFileName;
            }
        }
        return "unknown";
    }

    private void writeFile(byte[] content, String filename) throws IOException {

        File file = new File(filename);

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fop = new FileOutputStream(file);

        fop.write(content);
        fop.flush();
        fop.close();
    }
}
