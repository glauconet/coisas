package Dominio;

import java.util.Date;

/**
 * Created by 09534485845 on 28/01/2016.
 */
public class Nota {

    private Long id;
    private String nome;
    private String texto;
    private Integer tipo;
    private String data;
    private String url;

    public Nota(){}
    public Nota(Long id, String nome, String texto, Integer tipo, String data, String url) {
        this.id = id;
        this.nome = nome;
        this.texto = texto;
        this.tipo = tipo;
        this.data = data;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public String toString() {
        return "Dominio.Nota{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", texto='" + texto + '\'' +
                ", tipo=" + tipo +
                ", data=" + data +
                ", url='" + url + '\'' +
                '}';
    }
}
