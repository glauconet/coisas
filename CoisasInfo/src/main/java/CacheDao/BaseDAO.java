package CacheDao;

import java.sql.*;

/**
 * Created by Glauco on 10/02/2016.
 */
public class BaseDAO {



    public BaseDAO(){

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    protected Connection getConnection(){

        String url = "jdbc:mysql://mysql01.coisa8.hospedagemdesites.ws";
        //String url = "jdbc:mysql://10.32.18.35/coisa8";

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url,"coisa8","samurai01");
            //conn = DriverManager.getConnection(url,"root","samurai01");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static void main(String [] args ){

        //BaseDAO db = new BaseDAO();

       // Connection conn = db.getConnection();
        //System.out.println(conn);

        listaCoisas();
    }

    public static void listaCoisas(){

        BaseDAO db = new BaseDAO();

        Connection conn = db.getConnection();
        System.out.println(conn);
        try {
            PreparedStatement stmt = conn.prepareStatement("select id, nome  from coisa8.coisas");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){

                System.out.print(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
