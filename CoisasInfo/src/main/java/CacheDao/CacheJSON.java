package CacheDao; /**
 * Created by 09534485845 on 28/01/2016.
 */


import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import Model.Nota;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CacheJSON {

    private String caminho = null;
    private FileInputStream file = null;


    private Properties properties = new Properties();

    public CacheJSON() {

        try {

           // if(System.getProperty("os.name").equals(("Linux"))){
             //   caminho = "/home/09534485845/afiles/gitprojects/coisas/CoisasInfo/src/main/jsons/";
               // file = new FileInputStream("/home/09534485845/afiles/gitprojects/coisas/CoisasInfo/src/main/resources/propriedades.txt");
            //}
            //properties.load(file);     for(String key : properties.stringPropertyNames()) {     String value = properties.getProperty(key);   System.out.println(key + " => " + value);      }

           // if(System.getProperty("os.name").equals("MINGW64_NT-10.0")) {

                caminho = "D:\\03-Projetos\\CoisasProject\\CoisasInfo\\CoisasInfo\\src\\main\\jsons\\";
                //file = new FileInputStream("D:\\03-Projetos\\CoisasProject\\CoisasInfo\\CoisasInfo\\src\\main\\resources\\propriedades.txt");
            //}


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void atualizar(Nota notaPassada) {

        JSONObject jsonObject = new JSONObject();
        File dir = null;
        FileWriter writeFile = null;

        Nota notaExistente = this.ler("nota"+ notaPassada.getId()+".txt");
        try {
           writeFile = new FileWriter(caminho + "nota"+ notaPassada.getId()+".txt");

            notaExistente.setNome(notaPassada.getNome());
            notaExistente.setTexto(notaPassada.getTexto());

            //Armazena dados em um Objeto JSON
            jsonObject.put("id" ,notaExistente.getId());
            jsonObject.put("nome", notaPassada.getNome());
            jsonObject.put("texto", notaPassada.getTexto());
            jsonObject.put("tipo", notaExistente.getTipo());
            jsonObject.put("data", notaExistente.getData());
            jsonObject.put("url", notaExistente.getUrl());
            //Escreve no arquivo conteudo do Objeto JSON
            writeFile.write(jsonObject.toJSONString());
            writeFile.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public  void gravar(Nota nota) {
        JSONObject jsonObject = new JSONObject();
        FileWriter writeFile = null;
        File dir = null;
        long proximo;

        try{
            dir = new File(caminho);
            File[] filesList = dir.listFiles();
            proximo = filesList.length + 1;
            writeFile = new FileWriter(caminho + "nota" +proximo +".txt");
           //Armazena dados em um Objeto JSON
            jsonObject.put("id" ,""+proximo);
            jsonObject.put("nome", nota.getNome());
            jsonObject.put("texto", nota.getTexto());
            jsonObject.put("tipo", "nota");
            jsonObject.put("data", "01/01/2016");//new Date().toString());
            jsonObject.put("url", "/api/notas/"+proximo);
            //Escreve no arquivo conteudo do Objeto JSON
            writeFile.write(jsonObject.toJSONString());
            writeFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }    //Imprimne na Tela o Objeto JSON para vizualização
    // System.out.println(jsonObject);

    //Leia mais em: Leitura e Escrita de arquivos JSON em Java http://www.devmedia.com.br/leitura-e-escrita-de-arquivos-json-em-java/27663#ixzz3yZ47ZWi6

    public  Nota ler(String texto) {
        Nota nota = null;
        JSONObject jsonObject = null;
        JSONParser parser = new JSONParser();

        try {

            jsonObject = (JSONObject) parser.parse(new FileReader(caminho+texto));
            nota = new Nota();

            nota.setId((String) jsonObject.get("id"));
            nota.setNome((String) jsonObject.get("nome"));
            nota.setTexto((String) jsonObject.get("texto"));
            nota.setUrl((String) jsonObject.get("url"));
            nota.setTipo((String) jsonObject.get("tipo"));
            nota.setData((String) jsonObject.get("data"));

        } catch (FileNotFoundException e) {//Trata as exceptions que podem ser lançadas no decorrer do processo
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return nota;
    }

    public List<Nota> listarNotas(){

        List<Nota> listaNotas = new ArrayList<Nota>();

        File dir = new File(caminho);
        System.out.print(caminho);

        File[] filesList = dir.listFiles();
        for (File file : filesList) {
            if (file.isFile()) {
                System.out.println(file.getName());
                Nota nota = this.ler(file.getName());
                listaNotas.add(nota);
            }
        }

        return  listaNotas.isEmpty() ? null : listaNotas;
    }
}

//Leia mais em: Leitura e Escrita de arquivos JSON em Java http://www.devmedia.com.br/leitura-e-escrita-de-arquivos-json-em-java/27663#ixzz3yZ6hclo8



