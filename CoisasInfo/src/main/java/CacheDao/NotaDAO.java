package CacheDao;

import Dominio.Nota;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 10/02/16.
 */
public class NotaDAO extends BaseDAO {

    public ArrayList<Nota> getNotas() throws SQLException {
        ArrayList<Nota> notas = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = this.getConnection();
        try {
            stmt = conn.prepareStatement("select * from coisa8.coisas");
            ResultSet rs = stmt.executeQuery();

            notas= new ArrayList<Nota>();
            while (rs.next()) {
                Nota n = createNota(rs);
                notas.add(n);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(stmt !=null){
                stmt.close();
            }
            if(conn !=null){
                conn.close();
            }
        }
        return notas;
    }

    public Nota getNotaById(Long id) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        conn = getConnection();
        try {
            stmt = conn.prepareStatement("select * from coisa8.coisas where id=?");
            stmt.setLong(1, id);
            ResultSet rs = null;
            rs = stmt.executeQuery();
            if (rs.next()) {
                Nota n = createNota(rs);
                rs.close();
                return n;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException(e);
        }finally {
            if(stmt !=null){
                stmt.close();
            }
            if(conn !=null){
                conn.close();
            }
        }
        return null;
    }


    public Nota createNota(ResultSet rs) throws SQLException {
        Nota nota = new Nota();
        nota.setId(rs.getLong("id"));
        nota.setTipo(rs.getInt("tipo"));
        nota.setNome(rs.getString("nome"));
        nota.setData(rs.getString("data"));
        nota.setTexto(rs.getString("nota"));
        return  nota;
    }

    public void Save (Nota n) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        try{
            conn = getConnection();
            if(n.getId()==null) {
                stmt = conn.prepareStatement("" +
                    "insert into coisa8.coisas (nome,tipo,data,url,nota) values (?,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            }else{
                stmt = conn.prepareStatement("" +
                    "update coisa8.coisas set nome=?, tipo=?, data=?, url=?, nota=? where id=?");
            }

            stmt.setString(1,n.getNome());
            stmt.setInt(2,n.getTipo());
            stmt.setString(3,"2016-02-09 19:31:36");
            stmt.setString(4,"/coisa/nota");
            stmt.setString(5, n.getTexto());
            if(n.getId()!=null){//UPDATE
                stmt.setLong(6,n.getId());
            }
            int count = stmt.executeUpdate();
            if(count == 0){
                throw new SQLException("Erro ao inserir  nota");
            }
            if(n.getId() == null){
                Long id = getGeneratedId(stmt);
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new SQLException("Erro ao inserir  nota");
        }finally {
            if(stmt !=null){
                stmt.close();
            }
            if(conn !=null){
                conn.close();
            }
        }
    }

    public Long getGeneratedId(Statement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        Long id = null;
        if(rs.next()){
           id = rs.getLong(1);
        }
        return id;
    }

    public boolean delete(Long id) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.prepareStatement("delete from coisa8.coisas where id=?");
            stmt.setLong(1,id);
            int count = stmt.executeUpdate();
            boolean ok = count > 0;
            return ok;
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Erro ao apagar  nota");
        }finally {
            if(stmt !=null){
                stmt.close();
            }
            if(conn !=null){
                conn.close();
            }
        }
    }
}
