var Calculator = {
 
    somar : function(data) {
        var settings = {
            url : 'api/calculator/sum',
            type : 'POST',
            data : JSON.stringify(data),
            contentType : 'application/json'
        };
 
        return $.ajax(settings);
    },
    
    sumFailed : function(retorno) {
        if (retono.status === 442) {
            var data = JSON.parse(retorno.responseText);
            $.each(data, function(index, value) {
                $("#" + value.property).addClass("required");
           
            });
        }
        
        if (retorno.status === 500) {
            var retorno = JSON.parse(retorno.responseText);
            var inputId = retorno[0].property;
            $("#" + inputId).addClass("required");
        }
       
    }
};


