var Nota = {

    n1:{},
    notas:{},
    nota_atual:{},

    iniciar: function () {
      $('#nota_editar').hide();
      $('#nota_listar').show();
      $('#enviar_nota').on('click', Nota.enviar);
      $('#buscar_nota').on("click", Nota.buscar);
      $('#nova_nota').on("click", Nota.nova);
      $('#nota_listar_tabela').delegate('tr','click', Nota.editar);

    },

    editar: function(){
        $('#enviar_nota').on('click', Nota.enviar);
        for (var i = 0, len = Nota.notas.length; i < len; i++) {
            if(Nota.notas[i].id == $(this).attr("id")){
                Nota.nota_atual = Nota.notas[i];
            }
        }
        $('#nome_nota').val(Nota.nota_atual.nome);
        $('#texto_nota').val(Nota.nota_atual.texto);// falta setar tipo, url e data
        $('#nota_listar').hide();
        $('#nota_editar').show();
    },

    enviar: function () {
       // event.preventDefault();
        var dados_notas = {
            id: Nota.nota_atual.id,
            nome : $("#nome_nota").val(),
            tipo : "1",
            texto : $("#texto_nota").val()
        };
        Nota.inserir(dados_notas).done(Nota.inserirOk).fail(Nota.inserirFail);
    },

    enviar_novo: function () {
        var dados_notas = {
            nome : $("#nome_nota").val(),
            tipo:"1",
            texto : $("#texto_nota").val()
        };
        Nota.inserir(dados_notas).done(Nota.inserirOk).fail(Nota.inserirFail);
        $('#enviar_nota').off('click', Nota.enviar_novo);
        $('#enviar_nota').on('click', Nota.enviar);
    },

    inserir : function(nota) {

        var settings = {
            type : "POST",
            url : 'api/nota/inserir',
            data : JSON.stringify(nota),
            contentType : "application/json"
        };
        return $.ajax(settings);
    },

    inserirOk: function(data,  textStatus, jqXHR){
       //console.log(data.toString());
        console.log(textStatus);
        console.log(jqXHR.responseText);
        console.log(jqXHR.status);
        console.log(jqXHR.getResponseHeader('Location'));
        Nota.listar();
        $('#nota_editar').hide();
        $('#nota_listar').show();
    },

    inserirFail: function(request){

       /* $.each(request.responseJSON, function(index, value) {
            console.log(index);
            console.log(value);

        }); */
        switch (request.status) {
            case 422:
                $($("form input").get().reverse()).each(function() {
                    var id = $(this).attr('id');
                    var message = null;

                    $.each(request.responseJSON, function(index, value) {
                        if (id == value.property) {
                            message = value.message;
                            return;
                        }
                    });

                    if (message) {
                        console.log($("#" + id ).parent());
                        $("#" + id ).parent().addClass("has-error");
                        $("#" + id + "-message").html(message).show();
                        $(this).focus();
                    } else {
                        $("#" + id ).parent().removeClass("has-error");
                        $("#" + id + "-message").hide();
                    }
                });
                break;

            default:
                $("#global-message").addClass("alert-danger").text("Erro ao cadastrar pessoa.").show();
                break;
        }
    },

    obter : function(id){
        return $.ajax({
            type : "GET",
            url : this.url + "/" + id
        });
    },

    nova : function(){
        $('#enviar_nota').off('click', Nota.enviar);
        $('#enviar_nota').on('click', Nota.enviar_novo);
        $('#nota_listar').hide();
        Nota.nota_atual = null;
        $('#nome_nota').val("");
        $('#texto_nota').val("");
        $('#nota_editar').show();


    },
    listar : function(){
        Nota.buscar().done(Nota.buscarOk);


    },

    buscar : function(){
        return $.ajax({
            type : "GET",
            url : 'api/nota' // /listar'
        });
    },

    buscarOk:function(notas) {

        Nota.notas = notas;

        var table = $("#nota_listar_tabela");

        var body = $("#nota_listar_tbody")[0];

        $(body).empty();

        if (notas) {
            for (var i = 0; i < notas.length; i++) {

                var  notatmp = notas[i];

                var row = document.createElement('tr');

                var cellId = document.createElement('td');
                var textId = document.createTextNode(notatmp.id);
                cellId.appendChild(textId);

                var cellNome = document.createElement('td');
                var textNome = document.createTextNode(notatmp.nome);
                cellNome.appendChild(textNome);

                var cellTexto = document.createElement('td');
                var textTexto = document.createTextNode(notatmp.texto);
                cellTexto.appendChild(textTexto);

                var cellEmail = document.createElement('td');
                var textEmail = document.createTextNode(notatmp.tipo);
                cellEmail.appendChild(textEmail);

                var cellTelefone = document.createElement('td');
                var textTelefone = document.createTextNode(notatmp.url);
                cellTelefone.appendChild(textTelefone);

                var cellData = document.createElement('td');
                var textData = document.createTextNode(notatmp.data);
                cellData.appendChild(textData);


                row.setAttribute("id",notatmp.id);
                row.appendChild(cellId);
                row.appendChild(cellNome);
                row.appendChild(cellTexto);
                row.appendChild(cellEmail);
                row.appendChild(cellTelefone);
                row.appendChild(cellData);
                body.appendChild(row);
            }
        } else {
            var foot = document.createElement('tfoot');
            var emptyRow = document.createElement('tr');
            var emptyCell = document.createElement('td');
            var noRegisterText = document.createTextNode("Nenhum registro retornado");
            emptyCell.appendChild(noRegisterText);
            emptyCell.setAttribute("colspan", 4);
            emptyRow.appendChild(emptyCell);
            foot.appendChild(emptyRow);
            table.appendChild(foot);
        }
    }
}
