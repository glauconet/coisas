
var Inicio = {
    
	Tela:{},    //	Classex:{}, ClasseY:{},

	Iniciar: function() {
            Inicio.limpaTela();
            $('#navbar a').bind("click", Inicio.MontarTela2);
            Inicio.MontarTela1();
			//$('#enviar').bind('click', Nota.clicar);
    },

    MontarTela1: function(){
        Inicio.Tela.codigo=1;
        $('#container_home').show();

    },

	MontarTela2: function(event){
            event.preventDefault();
            Inicio.Tela.codigo=2;
            Inicio.Destino =   $(this).attr("id");
            Inicio.limpaTela();

            switch (Inicio.Destino){
                case "notas":
                    Nota.iniciar();
                    Nota.listar();
                    $('#container_nota').show();

                    break;
                case "fotos":
                    $('#tipo_arquivo').text(Inicio.Destino);
                    $('#container_upload').show();
                    break;
                case "sons":
                    $('#tipo_arquivo').text(Inicio.Destino);
                    $('#container_upload').show();
                    //$('#container_sons').show();
                    break;
                case "videos":
                    $('#tipo_arquivo').text(Inicio.Destino);
                    $('#container_upload').show();
                    //$('#container_videos').show();
                    break;
                case "figuras":
                    $('#tipo_arquivo').text(Inicio.Destino);
                    $('#container_upload').show();
                    //$('#container_figuras').show();
                    break;
                case "desenhos":
                    $('#tipo_arquivo').text(Inicio.Destino);
                    $('#container_upload').show();
                   // $('#container_desenhos').show();
                    break;
                case "conclusions":

                    $('#container_conclusions').show();
                    break;
                case "help":
                    $('#container_help').show();
                    break;
            }
	},

    limpaTela: function () {
            $('#container_home').hide();
            $('#container_nota').hide();
            $('#container_fotos').hide();
            $('#container_sons').hide();
            $('#container_videos').hide();
            $('#container_figuras').hide();
            $('#container_desenhos').hide();
            $('#container_conclusions').hide();
            $('#container_help').hide();
            $('#container_upload').hide();
            $('#container_listar_arquivos').hide();
            $('#arquivos_detalhe').hide();
    }
};

$(function () {
    Inicio.Iniciar();
});